# Why-Learn-Maths
Transcript to accompany three discussions with a Melbourne Montessori Cycle 3 class

I had been teaching the Melbourne Montessori School Olympiad Maths group for about a year and the teacher of one of the Cycle 3 classes 
(ages 9 to 12) asked me if I could speak to his class to share some of the insights that were
delighting and inspiring the Olympiad group. 

Many of these other students were struggling, or put off by maths, but each week there were
intrigued by the energy coming from my group during and after the group sessions. Perhaps I could build on that interest
and with some thoughtful examples help to inspire them.

I am not a teacher. I have been a professional mathematician and a management consultant, and have some experience trying to
explain complicated ideas. But the idea of trying to motivate an audience that would likely be unresponsive or even hostile
made me rather dubious.

I had also never actually questioned what maths was really about, why one might want to study it, or what gains ordinary people
could expect to get from studying mathematics. I had always enjoyed mathematics for its own sake, for the curiosity it inspired
in me, and because it reflected my general approach to understanding the world. But I had no idea what other people might find
interesting about it. Moreover, I suspected that for the most part the subject that I think of as mathematics - the highly
abstract and technical worlds constructed out of rules and logic - might not be possible to convey to students with only basic
arithmetic. And perhaps what I thought of as mathematics wasn't really of any interest or use to these people at all. Perhaps
what the teacher wanted was for me to motivate his kids to put more effort into studying arithmetic - which I had always seen
as a drudge totally unrelated to real maths.

I spent a long time discussing with a scientist friend and with the teacher what I could write about. I read a great deal of
blog posts, essays and talks on related themes, watched a bunch of TED talks, and eventually evolved some thoughts that I thought
would be both interesting and useful to a general audience of students. I also found an example of *real mathematics* that I thought
would be accessible and useful to them: fractals.

I am sure you will not agree with a good deal of what is in the transcript. Not all of it made it into the discussion with the students,
which was very freeform and ranged more broadly then the transcript. But perhaps if faced with a similar task, or simply to feed your
own curiosity, you might find some value in it.

I would love to hear your comments and suggestions for improving the content, whether you agree with what I have written or not.
Let me know if you would like access to any of the images or text and I can add them to this repository.


This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)
